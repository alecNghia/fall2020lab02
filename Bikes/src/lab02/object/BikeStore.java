package lab02.object;
import lab02.eclipse.Bicycle;

//Phan Hieu Nghia, 1834104

public class BikeStore {
	public static void main(String []args) {
		Bicycle[] bikes = new Bicycle[4];
		
		bikes[0] = new Bicycle("Trek",18,28);
		bikes[1] = new Bicycle("Canyon",21,40);
		bikes[2] = new Bicycle("Cannondale",24,45);
		bikes[3] = new Bicycle("Devinci",20,35);
		
		for(int i = 0; i < bikes.length; i++) {
			System.out.println(bikes[i]);
		}
	}
}
