package lab02.eclipse;

//Phan Hieu Nghia, 1834104

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	public int numberGears() {
		return numberGears;
	}
	
	public double maxSpeed() {
		return maxSpeed;
	}
	
	public String toString() {
		return "Manufacturer: " + this.manufacturer + ", Number of gears: " + this.numberGears + ", Max speed: " + this.maxSpeed;
	}
}

